package org.WeatherURL.Stepdef2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class E2E2_StepDefinations {

	WebDriver driver;

	@Given("^User has launched website$")
	public void LaunchBrowser() {
		System.setProperty("webdriver.chrome.driver",
				"E://Workspaces//org.WeatherURL.cucumber//drivers//chromedriver.exe");
		this.driver = new ChromeDriver();
		this.driver.get("https://openweathermap.org/");
	}

	@When("^Website has launched successfully$")
	public void E2E1_urlLoad() {
		this.driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

	}

	@Then("^User should be able to search for valid city$")
	public void searchCity() {

		this.driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		this.driver.manage().window().maximize();
		Actions tip = new Actions(this.driver);
		tip.click(this.driver.findElement(By.id("q"))).build().perform();
		tip.click(this.driver.findElement(By.xpath("//*[@id='searchform']/button"))).sendKeys("Mumbai").click().build().perform();
	}

}
