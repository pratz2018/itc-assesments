package org.WeatherURL.StepDef;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class E2E1_StepDefinations {

	WebDriver driver;


	@Given("^User has launched website$")
	public void LaunchBrowser() {
		System.setProperty("webdriver.chrome.driver",
				"E://Workspaces//org.WeatherURL.cucumber//drivers//chromedriver.exe");
		this.driver = new ChromeDriver();
		this.driver.get("https://openweathermap.org/");
	}

	@When("^Website has launched successfully$")
	public void E2E1_urlLoad() {
		this.driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

	}

	@Then("^User should be able to view landing page$")
	public void VerifyText() {
		WebElement webMainText = this.driver
				.findElement(By.xpath("//*[@id='main-slideshow']/div/div/div/div/div/div/h2"));
		WebElement webSubText = this.driver
				.findElement(By.xpath("//*[@id='main-slideshow']/div/div/div/div/div/div/h4"));
		Assert.assertEquals("We Deliver 2 Billion Forecasts Per Day", webMainText.getText());
		Assert.assertEquals("2,000 new subscribers a day | 1,700,000 customers | 20+ weather APIs",
				webSubText.getText());
		System.out.println("Text Value of Main Text on Screen is :" + webMainText.getText());
		System.out.println("Text Value of Main Text on Screen is :" + webSubText.getText());

	}

	@Then("^Header should be displayed with different Header options$")
	public void E2E1_verifyHeader() {
		List<WebElement> headers = this.driver
				.findElements(By.xpath("//*[@id='undefined-sticky-wrapper']/div/div/div/div[2]/ul"));

		String[] linkTexts = new String[headers.size()];
		int i = 0;
		for (WebElement e : headers) {
			System.out.println("Headers of Landing Page :" + e.getText());
			i++;

		}

	}
}
