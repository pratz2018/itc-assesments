package org.WeatherURL.testRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:Resources/E2E2.feature", glue = {
		"org.WeatherURL.Stepdef2"})
public class TestRunner_E2E2 {

}
